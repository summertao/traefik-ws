package socket

import (
	"errors"
	"io"
	"log"
	"net"
	"net/http"
	"reflect"
	"regexp"
	"time"

	"os"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const (
	// Log levels 0-4. Use to set the log level you wish to go for
	LogLevelError   = 0
	LogLevelWarning = 1
	LogLevelInfo    = 2
	LogLevelDebug   = 3

	// Sensible defaults for the socket
	defaultLogLevel                = LogLevelDebug
	defaultWriteWait               = 60 * time.Second
	defaultPongWait                = 30 * time.Second
	defaultPingPeriod              = defaultPongWait * 8 / 10
	defaultMaxMessageSize    int64 = 32 * 1024 * 1024
	defaultSendChannelBuffer       = 10
	defaultRecvChannelBuffer       = 10
	defaultAllowedOrigin           = ".*" // https?://{{host}}$
)

var (
	replacementRegexp = regexp.MustCompile("{{\\w+}}")
)

type Options struct {
	// The logger to use for socket logging
	Logger *log.Logger

	// The LogLevel for socket logging, goes from 0 (Error) to 3 (Debug)
	LogLevel int

	// Set to true if you want to skip logging
	SkipLogging bool

	// The time to wait between writes before timing out the connection
	// When this is a zero value time instance, write will never time out
	WriteWait time.Duration

	// The time to wait at maximum between receiving pings from the client.
	PongWait time.Duration

	// The time to wait between sending pings to the client
	PingPeriod time.Duration

	// The maximum messages size for receiving and sending in bytes
	MaxMessageSize int64

	// The send channel buffer
	SendChannelBuffer int

	// The receiving channel buffer
	RecvChannelBuffer int

	// Allowed origins for websocket connections
	AllowedOrigin string
}

type Connection struct {
	*Options

	// The websocket connection
	ws *websocket.Conn

	// The remote Address of the client using this connection. Cached on the
	// connection for logging.
	remoteAddr net.Addr

	// The error channel is given the error object as soon as an error occurs
	// either sending or receiving values from the websocket. This channel gets
	// mapped for the next handler to use.
	Error chan error

	// The disconnect channel is for listening for disconnects from the next handler.
	// Any sends to the disconnect channel lead to disconnecting the socket with the
	// given closing message. This channel gets mapped for the next
	// handler to use.
	Disconnect chan int

	// The done channel gets called only when the connection
	// has been successfully disconnected. Any sends to the disconnect
	// channel are currently ignored. This channel gets mapped for the next
	// handler to use.
	Done chan bool

	// The internal disconnect channel. Sending on this channel will lead to the handlers and
	// the connection closing.
	disconnect chan error

	// The disconnect send channel. Sending on this channel will lead to the send handler and
	// closing.
	disconnectSend chan bool

	// the ticker for pinging the client.
	ticker *time.Ticker
}

// Message Connection connects a websocket message connection to a []byte
// channel.
type ByteSliceConnection struct {
	*Connection

	// Sender is the []byte channel used for sending out bytes data to the client.
	// This channel gets mapped for the next handler to use and is asynchronous
	// unless the SendChannelBuffer is set to 0.
	Sender chan []byte

	// Receiver is the []byte channel used for receiving bytes data from the client.
	// This channel gets mapped for the next handler to use and is asynchronous
	// unless the RecvChannelBuffer is set to 0.
	Receiver chan []byte
}

// Generates a handler from an interface
func Start(c *gin.Context, o *Options) *ByteSliceConnection {
	if o == nil {
		o = newOptions(nil) // use default configs
	}
	// Upgrade the request to a websocket connection
	ws, status, err := upgradeRequest(c.Writer, c.Request, o)
	if err != nil {
		c.Writer.WriteHeader(status)
		c.Writer.Write([]byte(err.Error()))
		return nil
	}

	// Set up the connection
	bd := newBinding(ws, o)

	// Set the options for the gorilla websocket package
	bd.setSocketOptions()

	// start the send and receive goroutines
	go bd.send()
	go bd.recv()
	go waitForDisconnect(bd)

	return bd
}

// Log Level to strings slice
var LogLevelStrings = []string{"Error", "Warning", "Info", "Debug"}

// The options logger is only directly used while setting up the connection
// With the default logger, it logs in the format [socket][client remote address] log message
func (o *Options) log(message string, LogLevel int, logVars ...interface{}) {
	if LogLevel <= o.LogLevel && !o.SkipLogging {
		o.Logger.Printf("[%s] [%s] "+message, append([]interface{}{LogLevelStrings[LogLevel]}, logVars...)...)
	}
}

// The connection logger writes to the option logger using the cached remote address
// for this connection
func (c *Connection) log(message string, LogLevel int, logVars ...interface{}) {
	if LogLevel <= c.LogLevel {
		c.Options.log(message, LogLevel, append([]interface{}{c.remoteAddr}, logVars...)...)
	}
}

// Set the gorilla websocket handler options according to given options and set a default pong
// handler to keep the connection alive
func (c *Connection) setSocketOptions() {
	c.ws.SetReadLimit(c.MaxMessageSize)
	c.keepAlive()
	c.ws.SetPongHandler(func(string) error {
		c.log("Received Pong from Client", LogLevelDebug)
		c.keepAlive()
		return nil
	})
}

// Close the Base connection. Closes the send Handler and all channels used
// Since all channels are either internal or channels this middleware is sending on.
func (c *Connection) Close(closeCode int) error {
	c.disconnectSend <- true
	//TODO look for a better way to unblock the reader
	c.ws.SetReadDeadline(time.Now())

	// Send close message to the client
	c.log("Sending close message to client", LogLevelDebug)
	c.ws.WriteControl(websocket.CloseMessage, websocket.FormatCloseMessage(closeCode, ""), time.Now().Add(c.WriteWait))

	// If the connection can not be closed, return the error
	c.log("Closing websocket connection", LogLevelDebug)
	if err := c.ws.Close(); err != nil {
		c.log("Connection could not be closed: %s", LogLevelError, err.Error())
		return err
	}

	// Send disconnect message to the next handler
	c.log("Sending disconnect to handler", LogLevelDebug)
	c.Done <- true

	// Close disconnect and error channels this connection was sending on
	close(c.Done)
	close(c.Error)

	return nil
}

// Ping the client through the websocket
func (c *Connection) ping() error {
	c.log("Pinging socket", LogLevelDebug)
	return c.ws.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(c.WriteWait))
}

// Start the ticker used for pinging the client
func (c *Connection) startTicker() {
	c.log("Pinging every %v, first at %v", LogLevelDebug, c.PingPeriod, time.Now().Add(c.PingPeriod))
	c.ticker = time.NewTicker(c.PingPeriod)
}

// Stop the ticker used for pinging the client
func (c *Connection) stopTicker() {
	c.log("Stopped pinging socket", LogLevelDebug)
	c.ticker.Stop()
}

// Keep the connection alive by refreshing the deadlines.
func (c *Connection) keepAlive() {
	c.log("Setting read deadline to %v", LogLevelDebug, time.Now().Add(c.PongWait))
	c.ws.SetReadDeadline(time.Now().Add(c.PongWait))
	if c.WriteWait == 0 {
		c.log("Write deadline set to 0, will never expire", LogLevelDebug)
		c.ws.SetWriteDeadline(time.Time{})
	} else {
		c.log("Setting write deadline to %v", LogLevelDebug, time.Now().Add(c.WriteWait))
		c.ws.SetWriteDeadline(time.Now().Add(c.WriteWait))
	}
}

func (c *Connection) disconnectChannel() chan error {
	return c.disconnect
}

func (c *Connection) DisconnectChannel() chan int {
	return c.Disconnect
}

func (c *Connection) ErrorChannel() chan error {
	return c.Error
}

// Close the ByteSlice connection. Closes the send goroutine and all channels used
// Except for the send channel, since it should be closed by the handler sending on it.
func (c *ByteSliceConnection) Close(closeCode int) error {
	// Call close on the base connection
	c.log("Closing websocket connection", LogLevelDebug)
	err := c.Connection.Close(closeCode)

	if err != nil {
		return err
	}

	// Do not close the receiver here since it would send nil
	// Just let go
	c.log("Connection closed", LogLevelInfo)

	return nil
}

// Write the bytes to the websocket, also keeping the connection alive
func (c *ByteSliceConnection) write(mt int, payload []byte) error {
	c.keepAlive()
	return c.ws.WriteMessage(mt, payload)
}

// Send handler for the ByteSlice connection. Starts a goroutine
// Listening on the sender channel and writing received strings
// to the websocket.
func (c *ByteSliceConnection) send() {
	// Start the ticker and defer stopping it and decrementing the
	// wait group counter.
	c.startTicker()
	defer func() {
		c.stopTicker()
		c.log("Goroutine sending to websocket has been closed", LogLevelDebug)
	}()

	for {
		select {
		// Receiving a message from the next handler
		case message, ok := <-c.Sender:
			if !ok {
				c.log("Sender channel has been closed", LogLevelError)
				c.disconnect <- errors.New("Sender channel has been closed")
				return
			}
			// Write the message as a byte array to the socket
			c.log("Writing %s to socket", LogLevelDebug, string(message))
			if err := c.write(websocket.BinaryMessage, message); err != nil {
				c.log("Error writing to socket: %s", LogLevelError, err)
				c.disconnect <- err
				return
			}

			c.keepAlive()
			// Ping the client
		case <-c.ticker.C:
			err := c.ping()
			c.log("%s", LogLevelDebug, err)
			if err := c.ping(); err != nil {
				c.log("Error pinging socket: %s", LogLevelError, err)
				c.disconnect <- err
				return
			}

			// Receiving disconnectSend from the closing Connection
		case <-c.disconnectSend:
			return
		}
	}
}

func (c *ByteSliceConnection) recv() {
	// Defer decrementing the wait group counter and closing the connection
	defer func() {
		c.log("Goroutine receiving from websocket has been closed", LogLevelDebug)
	}()

	for {
		// Read a message from the client
		_, message, err := c.ws.ReadMessage()
		if err != nil {
			c.log("Error reading from socket: %s", LogLevelError, err)
			c.disconnect <- err
			return
		}
		// Send the message as a string to the next handler
		c.log("Read message from socket, %s", LogLevelDebug, string(message))
		c.Receiver <- message
		c.keepAlive()
	}
}

// Waits for a disconnect message and closes the connection with an appropriate close message.
// The possible messages are:
// TODO this should get more elaborate.
// CloseNormalClosure           = 1000
// CloseGoingAway               = 1001
// CloseProtocolError           = 1002
// CloseUnsupportedData         = 1003
// CloseNoStatusReceived        = 1005
// CloseAbnormalClosure         = 1006
// CloseInvalidFramePayloadData = 1007
// ClosePolicyViolation         = 1008
// CloseMessageTooBig           = 1009
// CloseMandatoryExtension      = 1010
// CloseInternalServerErr       = 1011
// CloseTLSHandshake            = 1015
func waitForDisconnect(c Binding) {
	for {
		select {
		case err := <-c.disconnectChannel():
			if err == io.EOF {
				c.ErrorChannel() <- err
				c.Close(websocket.CloseNormalClosure)
			} else {
				c.Close(websocket.CloseAbnormalClosure)
			}

			return
		case closeCode := <-c.DisconnectChannel():
			c.Close(closeCode)
			return
		}
	}
}

type Binding interface {
	Close(int) error
	recv()
	send()
	setSocketOptions()
	disconnectChannel() chan error
	DisconnectChannel() chan int
	ErrorChannel() chan error
}

// Creates a new JSON Connection
func newBinding(ws *websocket.Conn, o *Options) *ByteSliceConnection {
	return &ByteSliceConnection{
		newConnection(ws, o),
		make(chan []byte, o.SendChannelBuffer),
		make(chan []byte, o.RecvChannelBuffer),
	}
}

// Creates a new Connection
func newConnection(ws *websocket.Conn, o *Options) *Connection {
	return &Connection{
		o,
		ws,
		ws.RemoteAddr(),
		make(chan error, 1),
		make(chan int, 1),
		make(chan bool, 3),
		make(chan error, 1),
		make(chan bool, 1),
		nil,
	}
}

// Creates new default options and assigns any given options
func newOptions(options []*Options) *Options {
	o := Options{
		log.New(os.Stderr, "[sockets] ", 0),
		defaultLogLevel,
		false,
		defaultWriteWait,
		defaultPongWait,
		defaultPingPeriod,
		defaultMaxMessageSize,
		defaultSendChannelBuffer,
		defaultRecvChannelBuffer,
		defaultAllowedOrigin,
	}

	// when all defaults, return it
	if len(options) == 0 {
		return &o
	}

	// map the given values to the options
	optionsValue := reflect.ValueOf(options[0])
	oValue := reflect.ValueOf(&o)
	numFields := optionsValue.Elem().NumField()

	for i := 0; i < numFields; i++ {
		if value := optionsValue.Elem().Field(i); value.IsValid() && value.CanSet() && isNonEmptyOption(value) {
			oValue.Elem().Field(i).Set(value)
		}
	}

	return &o
}

// Upgrade the connection to a websocket connection
func upgradeRequest(resp http.ResponseWriter, req *http.Request, o *Options) (*websocket.Conn, int, error) {
	if req.Method != "GET" {
		o.log("Method %s is not allowed", LogLevelWarning, req.RemoteAddr, req.Method)
		return nil, http.StatusMethodNotAllowed, errors.New("Method not allowed")
	}

	allowedOrigin := replacementRegexp.ReplaceAllString(o.AllowedOrigin, req.Host)
	if r, err := regexp.MatchString(allowedOrigin, req.Header.Get("Origin")); !r || err != nil {
		o.log("Origin %s is not allowed", LogLevelWarning, req.RemoteAddr, req.Host)
		return nil, http.StatusForbidden, errors.New("Origin not allowed")
	}

	o.log("Request to %s has been allowed for origin %s", LogLevelDebug, req.RemoteAddr, req.Host, req.Header.Get("Origin"))

	ws, err := websocket.Upgrade(resp, req, nil, 1024, 1024)
	if handshakeErr, ok := err.(websocket.HandshakeError); ok {
		o.log("Handshake failed: %s", LogLevelWarning, req.RemoteAddr, handshakeErr)
		return nil, http.StatusBadRequest, handshakeErr
	} else if err != nil {
		o.log("Handshake failed: %s", LogLevelWarning, req.RemoteAddr, err)
		return nil, http.StatusBadRequest, err
	}

	o.log("Connection established", LogLevelInfo, req.RemoteAddr)
	return ws, http.StatusOK, nil
}

func isNonEmptyOption(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.String:
		return v.Len() != 0
	case reflect.Bool:
		return v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() != 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() != 0
	case reflect.Float32, reflect.Float64:
		return v.Float() != 0
	case reflect.Interface, reflect.Ptr:
		return !v.IsNil()
	}
	return false
}
