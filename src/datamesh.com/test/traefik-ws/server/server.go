package main

import (
	_ "net/http/pprof"
	"strconv"


	"fmt"

	"github.com/gin-gonic/gin"
	"datamesh.com/test/traefik-ws/socket"
)

var port = ":12121"

func main() {
	//gin.SetMode(gin.ReleaseMode)
	gin.SetMode(gin.DebugMode)
	m := gin.New()
	m.Use(gin.Logger())
	m.Use(gin.Recovery())

	syncGroup := m.Group("/v1")
	{
		syncGroup.GET("/a", echo)
	}

	panic(m.Run(port))
}

//service for time synchronization.
func echo(c *gin.Context) {
	tenantId := c.Param("tenantId")

	conn := socket.Start(c, nil)
	if conn == nil {
		panic("failed to start websocket server.")
	}
	var ch = make(chan []byte, 100)

	for i := 0; i < 5; i++ {
		ch <- []byte(":----------------------------" + tenantId + ":" + port + " ------ " + strconv.Itoa(i))
	}

	for {
		select {
		case a := <-ch:
			b := a
			conn.Sender <- b
		case <-conn.Done:
			fmt.Println("conn.Done")
			return
		case err := <-conn.Error:
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println("conn.Error")
			return
		}
	}
}
