package main

import (
	"fmt"
	"net/url"

	"time"

	"github.com/gorilla/websocket"
)

func main() {

	for {
		do()
		//time.Sleep(time.Second * 5)
		fmt.Println()
	}
	ch := make(chan int)
	ch <- 1
}

func do() {
	u := url.URL{Scheme: "ws", Host: "127.0.0.1:80", Path: "/v1/a"}
	c := getConn(u.String())

	for i := 0; i < 5; i++ {
		_, msg, err := c.ReadMessage()
		if err != nil {
			fmt.Println("client read message error: ", err)
			c = getConn(u.String())
			continue
		}
		fmt.Println("client recv message: ", string(msg))
	}
	c.UnderlyingConn().Close()
	c.WriteControl(websocket.CloseMessage, []byte("close"), time.Now())

	c.Close()
}

func getConn(address string) *websocket.Conn {
	c, _, err := websocket.DefaultDialer.Dial(address, nil)
	if err == nil {
		return c
	}
	return c
}
